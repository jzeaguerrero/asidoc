package entity;

/**
 * Created by Jhoncer on 26/11/2016.
 */
public class ProfesorDTO {
    private int id_docente=0;
    private String cod_docente=null;
    private String nom_docente=null;
    private String ape_paterno=null;
    private String ape_materno=null;

    public ProfesorDTO() {
    }

    public ProfesorDTO(int id_docente, String cod_docente, String nom_docente, String ape_paterno){
        this.id_docente=id_docente;
        this.cod_docente=cod_docente;
        this.nom_docente=nom_docente;
        this.ape_paterno=ape_paterno;
    }

    public int getId_docente() {
        return id_docente;
    }

    public void setId_docente(int id_docente) {
        this.id_docente = id_docente;
    }

    public String getCod_docente() {
        return cod_docente;
    }

    public void setCod_docente(String cod_docente) {
        this.cod_docente = cod_docente;
    }

    public String getNom_docente() {
        return nom_docente;
    }

    public void setNom_docente(String nom_docente) {
        this.nom_docente = nom_docente;
    }

    public String getApe_paterno() {
        return ape_paterno;
    }

    public void setApe_paterno(String ape_paterno) {
        this.ape_paterno = ape_paterno;
    }

    public String getApe_materno() {
        return ape_materno;
    }

    public void setApe_materno(String ape_materno) {
        this.ape_materno = ape_materno;
    }
}
