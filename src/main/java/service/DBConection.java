package service;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by Jhoncer on 26/11/2016.
 */
public class DBConection {
    private static Connection cn = null;
    public static Connection getConnection() throws Exception {
        try {
            if (cn == null) {

                // carga driver en memoria
                Class.forName("org.postgresql.Driver").newInstance();
                // url de la base de datos
                String url = "jdbc:postgresql://localhost:5433/dbasidoc";
                // obtener la conexion a la base de datos
                cn = DriverManager.getConnection(url, "postgres", "jose2k12");
            }
        } catch (Exception ex) {
            throw ex;
        }
        return cn;
    }
}
