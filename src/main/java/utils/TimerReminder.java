package utils;

import view.RecognizeFaceView;

import javax.swing.*;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by MIGUEL ZEA on 3/12/2016.
 */
public class TimerReminder {
    Timer timer;
    JFrame jFrame;

    public TimerReminder(JFrame frame,int seconds) {
        jFrame=frame;
        timer = new Timer();
        timer.schedule(new RemindTask(), seconds*1000);
    }

    class RemindTask extends TimerTask {
        public void run() {
            RecognizeFaceView recognizeFaceView = new RecognizeFaceView();
            jFrame.dispose();
            recognizeFaceView.setVisible(true);
            timer.cancel();
        }
    }
}
