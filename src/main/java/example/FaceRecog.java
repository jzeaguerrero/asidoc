package example;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamResolution;
import org.bytedeco.javacpp.opencv_core;
import org.opencv.core.*;
import org.opencv.highgui.VideoCapture;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.processing.face.detection.DetectedFace;
import org.openimaj.image.processing.face.detection.HaarCascadeDetector;
import org.openimaj.math.geometry.shape.Rectangle;
import utils.FaceRecognition;
import utils.TransformToImage;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static org.bytedeco.javacpp.opencv_core.cvGetSeqElem;

/**
 * Created by MIGUEL ZEA on 27/11/2016.
 */
public class FaceRecog extends JFrame implements Runnable, WebcamPanel.Painter {

    private static final long serialVersionUID = 1L;

    private static final Executor EXECUTOR = Executors.newSingleThreadExecutor();
    private static final HaarCascadeDetector detector = new HaarCascadeDetector();
    private static final Stroke STROKE = new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 1.0f, new float[] { 1.0f }, 0.0f);

    private Webcam webcam = null;
    private WebcamPanel.Painter painter = null;
    private List<DetectedFace> faces = null;
    FaceRecognition reconocer = FaceRecognition.getInstance();


    public FaceRecog(){
        webcam = Webcam.getDefault();
        webcam.setViewSize(WebcamResolution.VGA.getSize());
        webcam.open(true);
        WebcamPanel panel = new WebcamPanel(webcam, false);
        panel.setPreferredSize(WebcamResolution.VGA.getSize());
        panel.setPainter(this);
        panel.setFPSDisplayed(true);
        panel.setFPSLimited(true);
        panel.setFPSLimit(20);
        panel.setPainter(this);
        panel.setMirrored(true);
        panel.start();

        painter = panel.getDefaultPainter();

        add(panel);

        setTitle("Face Detector Example");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);

        EXECUTOR.execute(this);
    }

    public void paintPanel(WebcamPanel webcamPanel, Graphics2D graphics2D) {
        if (painter != null) {
            painter.paintPanel(webcamPanel,graphics2D);
        }
    }

    public void paintImage(WebcamPanel webcamPanel, BufferedImage bufferedImage, Graphics2D graphics2D) {
        if (painter != null) {
            painter.paintImage(webcamPanel, bufferedImage, graphics2D);
        }

        if (faces == null) {
            return;
        }

        Iterator<DetectedFace> dfi = faces.iterator();
        while (dfi.hasNext()) {
            DetectedFace face = dfi.next();
            Rectangle bounds = face.getBounds();

            int dx = (int) (0.1 * bounds.width);
            int dy = (int) (0.2 * bounds.height);
            int x = (int) bounds.x - dx;
            int y = (int) bounds.y - dy;
            int w = (int) bounds.width + 2 * dx;
            int h = (int) bounds.height + dy;

            graphics2D.setStroke(STROKE);
            graphics2D.setColor(Color.RED);
            graphics2D.drawRect(x, y, w, h);
            BufferedImage image = webcam.getImage();
            opencv_core.IplImage target = TransformToImage.toIplImage(image);
            opencv_core.CvSeq faces2 = reconocer.detectFace(target);
            opencv_core.CvRect r2 = new opencv_core.CvRect(cvGetSeqElem(faces2,0));
            target=reconocer.preprocessImage(target, r2);
            String namePerson = reconocer.identifyFace(target);
            if(!namePerson.equals("")) {

            }
        }
    }

    public void run() {
        while (true) {
            if (!webcam.isOpen()) {
                return;
            }
            faces = detector.detectFaces(ImageUtilities.createFImage(webcam.getImage()));
        }
    }

    public static void main(String[] args) throws IOException {
        new FaceRecog();
    }
}
