
package controller;

import entity.ProfesorDTO;
import service.DBConection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jhoncer
 */
public class ProfesorController {
    List lista=null;
    Connection con;
    Statement stm = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    String sql = null;
    int ok;
    List<ProfesorDTO> listaProf = null;
    ProfesorDTO prof = null;
    int cont=1;



    public List<ProfesorDTO> listaProfesores() throws Exception {
        lista = new ArrayList<ProfesorDTO>();
        try {
            con = DBConection.getConnection();
            sql = "select  * from dbasidoc.docente";
            stm = con.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                prof = new ProfesorDTO();
                prof.setId_docente(rs.getInt("id_docente"));
                prof.setCod_docente(rs.getString("cod_docente"));
                prof.setNom_docente(rs.getString("nom_docente"));
                prof.setApe_paterno(rs.getString("ape_paterno"));
                prof.setApe_materno(rs.getString("ape_materno"));
                lista.add(prof);
            }
            rs.close();
            stm.close();

        } catch (Exception e) {
            // TODO: handle exception
            throw e;
        } finally {
            // cn.close();
        }
        return lista;
    }

    public ProfesorDTO busqProfxCod(String codProfesor) throws Exception {
        prof = null;
        System.out.println("codProfesor = " + codProfesor);
        try {
            con = DBConection.getConnection();
            System.out.println("Conexion correcta");
            ps = con.prepareStatement("select * from dbasidoc.docente where cod_docente=?");

            ps.setString(1,codProfesor);
            rs = ps.executeQuery();
            if (rs.next()) {
                prof = new ProfesorDTO();
                prof.setId_docente(rs.getInt("id_docente"));
                prof.setCod_docente(rs.getString("cod_docente"));
                prof.setNom_docente(rs.getString("nom_docente"));
                prof.setApe_paterno(rs.getString("ape_paterno"));
                prof.setApe_materno(rs.getString("ape_materno"));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            System.out.println("Error: "+e);
            throw e;
        } finally {
            // cn.close();
        }
        return prof;
    }

    public ProfesorDTO busqProfxNomb(String nombreProfesor) throws Exception {
        prof = null;
        try {
            con = DBConection.getConnection();
            System.out.println("Conexion correcta");
            ps = con.prepareStatement("select * from dbasidoc.docente where nom_docente=?");

            ps.setString(1,nombreProfesor);
            rs = ps.executeQuery();
            if (rs.next()) {
                prof = new ProfesorDTO();
                prof.setId_docente(rs.getInt("id_docente"));
                prof.setCod_docente(rs.getString("cod_docente"));
                prof.setNom_docente(rs.getString("nom_docente"));
                prof.setApe_paterno(rs.getString("ape_paterno"));
                prof.setApe_materno(rs.getString("ape_materno"));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            System.out.println("Error: "+e);
            throw e;
        } finally {
            // cn.close();
        }
        return prof;
    }

    public boolean registrarAsistencia(ProfesorDTO prof, int tipo) throws SQLException{
        try {
            con = DBConection.getConnection();
//        Statement stm = con.createStatement();
            Date today= new Date();
            System.out.println("Profesor: "+prof.getNom_docente()+new Date().toString());
            int id_horario_curso=2;

            //Insertar en tabla Asistencia
            PreparedStatement ps = con.prepareStatement("INSERT INTO dbasidoc.asistencia VALUES (?,?,?,?,?);");
            cont=obtenerId();
            cont++;
            cont=obtenerId();
            ps.setInt(1,cont);//prof.getid_horarioUsuario
            ps.setInt(2,1);//prof.getid_horarioUsuario
            ps.setString(3, "E");//prof.getid_horarioUsuario
            ps.setDate(4,new java.sql.Date(today.getTime()));//prof.getid_horarioUsuario
            ps.setInt(5, tipo);
            ok=ps.executeUpdate();

            if(ok>0){
                ps = con.prepareStatement("INSERT INTO dbasidoc.asistencia_detalle VALUES (?, ?, ?, ?, ?,?);");
                ps.setInt(1, cont);
                ps.setInt(2, 1);
                ps.setString(3, "E");
                ps.setString(4, "F");
                ps.setDate(5,new java.sql.Date(today.getDate()));
                ps.setString(6, "C:\\tutor_assistance\\data\\images\\verify\\"+prof.getNom_docente()+"_"+prof.getApe_paterno()+".jpg");
                ps.executeUpdate();
            }
            return true;
        } catch (Exception ex) {
            Logger.getLogger(ProfesorController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public int obtenerId(){
        String sql="select id_asistencia from dbasidoc.asistencia";
        int nro=1;
        Statement stm;
        try {
            stm = con.createStatement();
            rs = stm.executeQuery(sql);
            while(rs.next()){
                nro = rs.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ex = " + ex);
            Logger.getLogger(ProfesorController.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            nro++;
            System.out.println("nro = " + nro);
            return nro;
        }
    }
}
